import Parser

type Nom = String

data Expression = Lam Nom Expression
                | App Expression Expression
                | Var Nom
                | Lit Litteral
                deriving (Show,Eq)

data Litteral = Entier Integer
              | Bool   Bool
              deriving (Show,Eq)

espacesP :: Parser()
espacesP = many (car ' ') >>= (\_ -> pure())

espacesP' :: Parser String
espacesP' = many (car ' ')

--Q2
nomP :: Parser Nom
nomP = some (carQuand (`elem` (['a'..'z']))) >>= (\nom -> espacesP >> pure nom)

--Q3
varP :: Parser Expression
varP = nomP >>= (\nom -> pure (Var nom))

--Q4
appliqueRec :: [Expression] -> Expression
appliqueRec [] = error "liste vide"
appliqueRec [x] = x
appliqueRec [x,y] = App x y
appliqueRec (x1:x2:xs) = App (App x1 x2) (applique xs)

--Prof
applique_recursive :: [Expression] -> Expression
applique_recursive [] = error "La liste est vide"
applique_recursive (x:[]) = x
applique_recursive (x:y:ys)= applique_recursive ((App x y):ys)

applique :: [Expression] -> Expression
applique = foldl1 App

--Q5
--exprP :: Parser Expression
--exprP = varP

exprsP :: Parser Expression
exprsP = (some exprP) >>= (\liste -> pure (applique liste))

-- Q6
-- outils
barreP :: Parser ()
barreP = car '\\' >>= (\ _ -> espacesP)

flecheP :: Parser ()
flecheP = chaine "->" >>= (\ _ -> espacesP)

lambdaP :: Parser Expression
lambdaP = barreP >> nomP >>= (\nom -> flecheP >> exprP >>= (\expr -> pure (Lam nom expr)))

-- avec la syntaxe do

lambdaP' :: Parser Expression
lambdaP' = do
  barreP
  nom <- nomP
  flecheP
  expr <- exprP
  pure (Lam nom expr)

--Q7
exprP :: Parser Expression
exprP = varP  <|> lambdaP <|> exprParentheseeP <|> nombreP <|> booleenP

--Q8
exprParentheseeP :: Parser Expression
exprParentheseeP = car '(' >> exprsP >>= (\e -> car ')' >> pure(e))

--Q9
nombreP :: Parser Expression
nombreP = some (carQuand (\c -> c `elem` ['0'..'9'])) >>= (\n -> espacesP >> pure ( Lit ( Entier (read(n)) ) ))

--Q10
booleenP :: Parser Expression
booleenP = (chaine "True" >>= (\_ -> pure(Lit (Bool True)) ) ) <|> (chaine "False" >>= (\_ -> pure (Lit (Bool False))))

--Q11
expressionP :: Parser Expression
expressionP = espacesP >> exprsP

--Q12
ras :: String -> Expression
ras s = resultat (runParser expressionP s)

--Q13
data ValeurA = VLitteralA Litteral
             | VFonctionA (ValeurA -> ValeurA)

instance Show ValeurA where
  show (VFonctionA _) = "λ"
  show (VLitteralA (Entier l) ) = show l
  show (VLitteralA (Bool b)) = show b

type Environnement a = [(Nom, a)]

interpreteA :: Environnement ValeurA -> Expression -> ValeurA
interpreteA _ (Lit a) = VLitteralA a
interpreteA env (Var a) = go where
  (Just go) = lookup a env
interpreteA env (Lam nom (Var b) ) = 
interpreteA env (App f e) = (interpreteA f) (interpreteA env e)


main :: IO ()
main = print("yo")
